#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>

struct block_header* block_get_header(void* contents);
//  - Обычное успешное выделение памяти.
//  - Освобождение одного блока из нескольких выделенных.
//  - Освобождение двух блоков из нескольких выделенных.
//  - Память закончилась, новый регион памяти расширяет старый.
//  - Память закончилась, старый регион памяти не расширить из-за другого выделенного 

bool test1();

bool test2();

bool test3();

bool test4();
bool test5();