#include "test.h"
#define HEAP_SIZE 999

#define BLOCK_SIZE 123
struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
//  - Обычное успешное выделение памяти.
//  - Освобождение одного блока из нескольких выделенных.
//  - Освобождение двух блоков из нескольких выделенных.
//  - Память закончилась, новый регион памяти расширяет старый.
//  - Память закончилась, старый регион памяти не расширить из-за другого выделенного 

bool test1() {
    void* heap = heap_init(HEAP_SIZE);
    void* malloced = _malloc(BLOCK_SIZE);
    if(!heap || !malloced) return false;
    debug_heap(stderr, heap);
    _free(malloced);
    munmap(heap, ((struct block_header*) heap)->capacity.bytes + offsetof(struct block_header, contents));
    return true;
}

bool test2() {
    void* heap = heap_init(HEAP_SIZE);

    void* a1 = _malloc(BLOCK_SIZE);
    
    void* a2 = _malloc(BLOCK_SIZE);


    void* a3 = _malloc(BLOCK_SIZE);

    if(!a1 || !a2 || !a3 || !heap) return false;
    _free(a2);
    debug_heap(stderr, heap);


    struct block_header* header = block_get_header(a2);


    if(!header->is_free || header->capacity.bytes != BLOCK_SIZE) return false;
    _free(a3);
    _free(a1);

    munmap(heap, ((struct block_header*) heap)->capacity.bytes + offsetof( struct block_header, contents ));
    return true;
}

bool test3() {
    void* heap = heap_init(HEAP_SIZE);
    void* a1 = _malloc(BLOCK_SIZE);
    void* a2 = _malloc(BLOCK_SIZE);
    void* a3 = _malloc(BLOCK_SIZE);
    if(!a1 || !a2 || !a3 || !heap) return false;
    _free(a1);
    _free(a2);
    debug_heap(stderr, heap);
    struct block_header* header = block_get_header(a2);
    struct block_header* header1 = block_get_header(a1);

    if(!header->is_free || header->capacity.bytes != BLOCK_SIZE ||
    !header1->is_free || header1->capacity.bytes != BLOCK_SIZE) return false;
    _free(a3);    
    //to make a1 a2 a3 one block
    _free(a1);


    munmap(heap, ((struct block_header*) heap)->capacity.bytes + offsetof( struct block_header, contents ));
    return true;
}

bool test4() {
    void* heap = heap_init(REGION_MIN_SIZE);
        debug_heap(stderr, heap);

    void* a1 = _malloc(REGION_MIN_SIZE*2/3);

    void* a2 = _malloc(REGION_MIN_SIZE*2/3);
    if(!a1 || !a2 || !heap) return false;

    struct block_header* header = block_get_header(a2);
    struct block_header* header1 = block_get_header(a1);
    debug_heap(stderr, heap);
    
    if(header->is_free || header->capacity.bytes != REGION_MIN_SIZE*2/3 ||
    header1->is_free || header1->capacity.bytes != REGION_MIN_SIZE*2/3) return false;

    _free(a2);
    _free(a1);

    //debug_heap(stderr, heap);

    munmap(heap, ((struct block_header*) heap)->capacity.bytes + offsetof( struct block_header, contents ));
    return true;
}

bool test5() {
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stderr, heap);

    struct block_header* header = (struct block_header*) heap;
    //делаем так чтобы не влезло в heap
    void* heap2 = mmap((void*) header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE , -1, 0 );
    if(heap2 == MAP_FAILED) return false;


    void* a1 = _malloc(REGION_MIN_SIZE * 2 / 3);
    if(!a1 || !heap) return false;
    debug_heap(stderr, heap);


    void* a2 = _malloc(REGION_MIN_SIZE * 2 / 3);
    header = block_get_header(a2);
    debug_heap(stderr, heap);
      if(header->is_free || header->capacity.bytes != REGION_MIN_SIZE * 2 / 3) return false;
    _free(a2);
    _free(a1);

    munmap(heap, ((struct block_header*) heap)->capacity.bytes + offsetof( struct block_header, contents ));
    munmap(heap2, REGION_MIN_SIZE);

    return true;




}