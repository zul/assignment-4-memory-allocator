#include "test.h"

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>



int main() {
    if(!test1()) printf("%s", "test 1 not passed\n");
    if(!test2()) printf("%s", "test 2 not passed\n");
    if(!test3()) printf("%s", "test 3 not passed\n");
    if(!test4()) printf("%s", "test 4 not passed\n");
    if(!test5()) printf("%s", "test 5 not passed\n");

    return 0;
}
